// untuk menchecklist pada bagian pemilihan gender
document.addEventListener("DOMContentLoaded", function() {
    let radiobuttons = document.querySelectorAll('input[name="jenisKelamin"]');
    radiobuttons.forEach(radio => {
        radio.addEventListener('mousedown', function(event) {
            if (this.checked) {
                this.addEventListener('click', function(event) {
                    this.checked = false;
                },{once: true});
            }
        });
    });
});


function validateForm(event){
    event.preventDefault(); // prevent default form submission behavior
    let nama = document.getElementById('nama').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let hobi = document.getElementById('hobi').value;
    let alamat = document.getElementById('kota').value; 
    let bio = document.getElementById('bio').value;
    let jenisKelaminPria = document.getElementById('pria').checked; 
    let jenisKelaminWanita = document.getElementById('wanita').checked; 


    if (!jenisKelaminPria && !jenisKelaminWanita) {
        alert('Jenis Kelamin harus dipilih');
        return false;
    }
    if (nama.length < 3) {
        alert('Nama harus lebih dari 3 karakter');
        return false;
    }
    if (!validateEmail(email)) {
        alert('Email tidak valid');
        return false;
    }
    if (password.length < 6) {
        alert('Password harus lebih dari 6 karakter');
        return false;
    }
    if (hobi.length < 3) {
        alert('Hobi harus lebih dari 3 karakter');
        return false;
    }
    if (alamat.length < 3) {
        alert('Alamat harus lebih dari 3 karakter');
        return false;
    }

    alert('Form telah berhasil tersubmit');
    return true;
}

function validateEmail(email) {
    const re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    return re.test(email);
}
